﻿using ProtoBuf;

namespace HoloServer.ProtocolMessages
{
    [ProtoContract]
    public class PlayerDisconnect
    {
        [ProtoMember(1)]
        public long Id { get; set; }
    }
}

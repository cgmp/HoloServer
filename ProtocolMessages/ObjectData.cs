using System.Collections.Generic;
using ProtoBuf;
namespace HoloServer.ProtocolMessages {
    [ProtoContract]
    public class ObjectData 
    {
        [ProtoMember(1)]
        public ulong Id { get; set; }

        [ProtoMember(2)]
        public string ObjectName { get; set; }


        [ProtoMember(3)]
        public Vector3 Position { get; set; }
        [ProtoMember(4)]
        public Quaternion Rotation { get; set; }

        [ProtoMember(5)]
        public Dictionary<string, string> SyncableProperties { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Runtime.Loader;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using HoloServer.PluginAPI;
using HoloServer.ProtocolMessages;
using Lidgren.Network;
using Microsoft.Extensions.Logging;
using HoloServer.PluginAPI.Events;

namespace HoloServer
{
    public class GameServer
    {
        public string Location => Directory.GetCurrentDirectory();
        public NetPeerConfiguration Config;

        public List<Client> Clients { get; set; }
        public int MaxPlayers { get; set; }
        public int Port { get; set; }
        public string GamemodeName { get; set; }
        public string Name { get; set; }
        public string Password { get; set; }
        public bool PasswordProtected => !string.IsNullOrEmpty(Password);
        //public string MasterServer { get; set; }
        //public string BackupMasterServer { get; set; }
        //public bool AnnounceSelf { get; set; }
        //public bool AllowNicknames { get; set; }
        public bool AllowOutdatedClients { get; set; }
        public readonly ScriptVersion ServerVersion = ScriptVersion.VERSION_0_9_3;
        public string LastKickedIP { get; set; }
        public Client LastKickedClient { get; set; }
        public bool DebugMode { get; set; }
        public NetServer Server;
        public int CurrentTick { get; set; } = 0;

        public int TicksPerSecond { get; set; }

        //private DateTime  nounceDateTime;
        private ILogger logger;
        private Dictionary<string, Action<object>> _callbacks = new Dictionary<string, Action<object>>();
        private int _ticksLastSecond;

        private Timer _tpsTimer;
        public GameServer(int port, string name, string gamemodeName, bool isDebug)
        {
            logger = Util.LoggerFactory.CreateLogger<GameServer>();
            logger.LogInformation("Server ready to start");
            Clients = new List<Client>();
            MaxPlayers = 32;
            GamemodeName = gamemodeName;
            Name = name;
            Port = port;
            //MasterServer = "http://46.101.1.92/";
            //BackupMasterServer = "https://gtamaster.nofla.me";
            Config = new NetPeerConfiguration("HoloSync") { Port = port };
            Config.EnableMessageType(NetIncomingMessageType.ConnectionApproval);
            Config.EnableMessageType(NetIncomingMessageType.DiscoveryRequest);
            Config.EnableMessageType(NetIncomingMessageType.UnconnectedData);
            Config.EnableMessageType(NetIncomingMessageType.ConnectionLatencyUpdated);
            Server = new NetServer(Config);

            logger.LogInformation($"NetServer created with port {Config.Port}");

            _tpsTimer = new Timer(state => CalculateTicksPerSecond(), null, 0, 1000);
        }

        public void Start()
        {
            logger.LogInformation("Server starting");

            logger.LogDebug("Loading gamemode");
            if (GamemodeName != "freeroam")
            {
                var assemblyName = Location + Path.DirectorySeparatorChar + GamemodeName + ".dll";
                var pluginAssembly = AssemblyLoadContext.Default.LoadFromAssemblyPath(assemblyName);
                var types = pluginAssembly.GetExportedTypes();
                var validTypes = types.Where(t => typeof(IGamemode).IsAssignableFrom(t)).ToArray();
                if (!validTypes.Any())
                {
                    logger.LogError("No gamemodes found in gamemode assembly, using freeroam");
                    GamemodeName = "freeroam";
                    return;
                }
                if (validTypes.Count() > 1)
                {
                    logger.LogError("Multiple valid gamemodes found in gamemode assembly, using freeroam");
                    GamemodeName = "freeroam";
                    return;
                }
                var gamemode = Activator.CreateInstance(validTypes.First()) as IGamemode;
                if (gamemode == null)
                {
                    logger.LogError(
                        "Could not create instance of gamemode (Activator.CreateInstance returned null), using freeroam");
                    GamemodeName = "freeroam";
                    return;
                }
                GamemodeName = gamemode.GamemodeName;
                gamemode.OnEnable(this, false);
            }
            logger.LogDebug("Gamemode loaded");
            Server.Start();
        }

        private void CalculateTicksPerSecond()
        {
            TicksPerSecond = CurrentTick - _ticksLastSecond;
            _ticksLastSecond = CurrentTick;

            //logger.LogTrace("TPS: " + TicksPerSecond);
            Console.Title = "HoloSync Server  - " + Name + " (" + Clients.Count + "/" + MaxPlayers + " players) - Port: " + Port + " - TPS: " + TicksPerSecond;
            return;
        }

        public void Tick()
        {
            CurrentTick++;
            GameEvents.Tick(CurrentTick);
            NetIncomingMessage msg;
            while ((msg = Server.ReadMessage()) != null)
            {
                Client client = null;
                lock (Clients)
                {
                    foreach (var c in Clients)
                    {
                        if (c?.NetConnection == null || c.NetConnection.RemoteUniqueIdentifier == 0 ||
                            msg.SenderConnection == null ||
                            c.NetConnection.RemoteUniqueIdentifier != msg.SenderConnection.RemoteUniqueIdentifier)
                            continue;
                        client = c;
                        break;
                    }
                }
                if (client == null)
                {
                    logger.LogDebug("Client not found for remote ID " + msg.SenderConnection?.RemoteUniqueIdentifier + ", creating client. Current number of clients: " + Clients.Count());
                    client = new Client(msg.SenderConnection);
                }

                // Plugin event: OnIncomingPacket
                var pluginPacketHandlerResult = PacketEvents.IncomingPacket(client, msg);
                msg = pluginPacketHandlerResult.Data;
                if (!pluginPacketHandlerResult.ContinueServerProc)
                {
                    Server.Recycle(msg);
                    return;
                }

                //logger.LogInformation("Packet received - type: " + ((NetIncomingMessageType)msg.MessageType).ToString());
                switch (msg.MessageType)
                {
                    case NetIncomingMessageType.UnconnectedData:
                        var ucType = msg.ReadString();
                        // ReSharper disable once ConvertIfStatementToSwitchStatement
                        if (ucType == "ping")
                        {
                            if (!PacketEvents.Ping(client, msg).ContinueServerProc)
                            {
                                Server.Recycle(msg);
                                return;
                            }
                            logger.LogInformation("Ping received from " + msg.SenderEndPoint.Address.ToString());
                            var reply = Server.CreateMessage("pong");
                            Server.SendMessage(reply, client.NetConnection, NetDeliveryMethod.ReliableOrdered);
                        }
                        else if (ucType == "query")
                        {
                            if (!PacketEvents.Query(client, msg).ContinueServerProc)
                            {
                                Server.Recycle(msg);
                                return;
                            }
                            var playersOnline = 0;
                            lock (Clients) playersOnline = Clients.Count;
                            logger.LogInformation("Query received from " + msg.SenderEndPoint.Address.ToString());
                            var reply = Server.CreateMessage($"{Name}%{PasswordProtected}%{playersOnline}%{MaxPlayers}%{GamemodeName}");
                            Server.SendMessage(reply, client.NetConnection, NetDeliveryMethod.ReliableOrdered);
                        }
                        break;
                    case NetIncomingMessageType.VerboseDebugMessage:
                    case NetIncomingMessageType.DebugMessage:
                        logger.LogDebug("Network (Verbose)DebugMessage: " + msg.ReadString());
                        break;
                    case NetIncomingMessageType.WarningMessage:
                        logger.LogWarning("Network WarningMessage: " + msg.ReadString());
                        break;
                    case NetIncomingMessageType.ErrorMessage:
                        logger.LogError("Network ErrorMessage: " + msg.ReadString());
                        break;
                    case NetIncomingMessageType.ConnectionLatencyUpdated:
                        client.Latency = msg.ReadFloat();
                        break;
                    case NetIncomingMessageType.ConnectionApproval:
                        var connectionApprovalPacketResult = PacketEvents.IncomingConnectionApproval(client, msg);
                        msg = connectionApprovalPacketResult.Data;
                        if (!connectionApprovalPacketResult.ContinueServerProc)
                        {
                            Server.Recycle(msg);
                            return;
                        }
                        HandleClientConnectionApproval(client, msg);
                        break;
                    case NetIncomingMessageType.StatusChanged:
                        pluginPacketHandlerResult = PacketEvents.IncomingStatusChange(client, msg);
                        msg = pluginPacketHandlerResult.Data;
                        if (!pluginPacketHandlerResult.ContinueServerProc)
                        {
                            Server.Recycle(msg);
                            return;
                        }
                        HandleClientStatusChange(client, msg);
                        break;
                    case NetIncomingMessageType.DiscoveryRequest:
                        pluginPacketHandlerResult = PacketEvents.IncomingDiscoveryRequest(client, msg);
                        msg = pluginPacketHandlerResult.Data;
                        if (!pluginPacketHandlerResult.ContinueServerProc)
                        {
                            Server.Recycle(msg);
                            return;
                        }
                        HandleClientDiscoveryRequest(client, msg);
                        break;
                    case NetIncomingMessageType.Data:
                        pluginPacketHandlerResult = PacketEvents.IncomingData(client, msg);
                        msg = pluginPacketHandlerResult.Data;
                        if (!pluginPacketHandlerResult.ContinueServerProc)
                        {
                            Server.Recycle(msg);
                            return;
                        }
                        HandleClientIncomingData(client, msg);
                        break;
                    default:
                        // We shouldn't get packets reaching this, so throw warnings when it happens.
                        logger.LogWarning("Unknown packet received: " +
                                          msg.MessageType.ToString());
                        break;

                }
                Server.Recycle(msg);
            }
        }
        private void HandleClientConnectionApproval(Client client, NetIncomingMessage msg)
        {
            var type = msg.ReadInt32();
            var length = msg.ReadInt32();
            var connReq = Util.DeserializeBinary<ConnectionRequest>(msg.ReadBytes(length));
            if (connReq == null)
            {
                DenyConnect(client, "Connection is null, this is most likely a bug in the client.", true, msg);
                return;
            }

            var pluginResponse = ConnectionEvents.ConnectionRequest(client, connReq);
            if (!pluginResponse.ContinueServerProc) return;
            connReq = pluginResponse.Data;

            client.DisplayName = connReq.DisplayName;
            client.Name = connReq.Name;
            client.GameVersion = connReq.GameVersion;
            client.RemoteScriptVersion = (ScriptVersion)connReq.ScriptVersion;

            client.DisplayName = client.NetConnection.RemoteEndPoint.Address.ToString();

            logger.LogInformation(
                $"New connection request: {client.DisplayName}@{msg.SenderEndPoint.Address.ToString()} | Game version: {client.GameVersion.ToString()} | Script version: {client.RemoteScriptVersion.ToString()}");

            var latestScriptVersion = Enum.GetValues(typeof(ScriptVersion)).Cast<ScriptVersion>().Last();
            if (!AllowOutdatedClients &&
                (ScriptVersion)connReq.ScriptVersion != latestScriptVersion)
            {
                var latestReadableScriptVersion = latestScriptVersion.ToString();
                latestReadableScriptVersion = Regex.Replace(latestReadableScriptVersion, "VERSION_", "",
                    RegexOptions.IgnoreCase);
                latestReadableScriptVersion = Regex.Replace(latestReadableScriptVersion, "_", ".",
                    RegexOptions.IgnoreCase);

                logger.LogInformation($"Client {client.DisplayName} tried to connect with an outdated script version {client.RemoteScriptVersion.ToString()} but the server requires {latestScriptVersion.ToString()}");
                DenyConnect(client, $"Please update to version ${latestReadableScriptVersion} of the client.", true, msg);
                return;
            }
            else if (client.RemoteScriptVersion != latestScriptVersion)
            {
                SendNotificationToPlayer(client, "You are currently on an outdated client. Please update your client.");
            }
            else if (client.RemoteScriptVersion == ScriptVersion.VERSION_UNKNOWN)
            {
                logger.LogInformation($"Client {client.DisplayName} tried to connect with an unknown script version (client too old?)");
                DenyConnect(client, $"Unknown version. Please re-download the client", true, msg);
                return;
            }
            var numClients = 0;
            lock (Clients) numClients = Clients.Count;
            if (numClients >= MaxPlayers)
            {
                logger.LogInformation($"Player tried to join while server is full: {client.DisplayName}");
                DenyConnect(client, "No available player slots.", true, msg);
            }

            if (PasswordProtected && connReq.Password != Password)
            {
                logger.LogInformation($"Client {client.DisplayName} tried to connect with the wrong password.");
                DenyConnect(client, "Wrong password.", true, msg);
            }

            lock (Clients)
                if (Clients.Any(c => c.DisplayName == client.DisplayName))
                {
                    DenyConnect(client, "A player already exists with the current display name.");
                }
                else
                {
                    Clients.Add(client);
                }



            var channelHail = Server.CreateMessage();
            channelHail.Write(GetChannelForClient(client));
            client.NetConnection.Approve(channelHail);
        }
        private void HandleClientStatusChange(Client client, NetIncomingMessage msg)
        {
            var newStatus = (NetConnectionStatus)msg.ReadByte();
            switch (newStatus)
            {
                case NetConnectionStatus.Connected:
                    logger.LogInformation($"Connected: {client.DisplayName}@{msg.SenderEndPoint.Address.ToString()}");
                    SendNotificationToAll($"Player connected: {client.DisplayName}");
                    break;

                case NetConnectionStatus.Disconnected:
                    lock (Clients)
                    {
                        if (Clients.Contains(client))
                        {
                            if (!client.Silent)
                            {
                                if (client.Kicked)
                                {
                                    if (string.IsNullOrEmpty(client.KickReason)) client.KickReason = "Unknown";
                                    SendNotificationToAll(
                                        $"Player kicked: {client.DisplayName} - Reason: {client.KickReason}");
                                }
                                else
                                {
                                    SendNotificationToAll(
                                        $"Player disconnected: {client.DisplayName}");
                                }
                            }
                            var dcMsg = new PlayerDisconnect()
                            {
                                Id = client.NetConnection.RemoteUniqueIdentifier
                            };

                            SendToAll(dcMsg, PacketType.PlayerDisconnect, true);

                            if (client.Kicked)
                            {
                                logger.LogInformation(
                                    $"Player kicked: {client.DisplayName}@{msg.SenderEndPoint.Address.ToString()}");
                                LastKickedClient = client;
                                LastKickedIP = client.NetConnection.RemoteEndPoint.ToString();
                            }
                            else
                            {
                                logger.LogInformation($"Player disconnected: {client.DisplayName}@{msg.SenderEndPoint.Address.ToString()}");
                            }
                            Clients.Remove(client);
                        }
                        break;
                    }
                // resharper was bugging me about not having the below case statements
                case NetConnectionStatus.None:
                case NetConnectionStatus.InitiatedConnect:
                case NetConnectionStatus.ReceivedInitiation:
                case NetConnectionStatus.RespondedAwaitingApproval:
                case NetConnectionStatus.RespondedConnect:
                case NetConnectionStatus.Disconnecting:
                default:
                    break;
            }
        }
        private void HandleClientDiscoveryRequest(Client client, NetIncomingMessage msg)
        {
            var responsePkt = Server.CreateMessage();
            var discoveryResponse = new DiscoveryResponse
            {
                ServerName = Name,
                MaxPlayers = MaxPlayers,
                PasswordProtected = PasswordProtected,
                Gamemode = GamemodeName,
                Port = Port,
            };
            lock (Clients) discoveryResponse.PlayerCount = Clients.Count;

            var serializedResponse = Util.SerializeBinary(discoveryResponse);
            responsePkt.Write((int)PacketType.DiscoveryResponse);
            responsePkt.Write(serializedResponse.Length);
            responsePkt.Write(serializedResponse);
            logger.LogInformation($"Server status requested by {msg.SenderEndPoint.Address.ToString()}");
            Server.SendDiscoveryResponse(responsePkt, msg.SenderEndPoint);
        }

        private void HandleClientIncomingData(Client client, NetIncomingMessage msg)
        {
            var packetType = (PacketType)msg.ReadInt32();

            switch (packetType)
            {
                case PacketType.ChatData:
                    {
                        logger.LogDebug("Received ChatData but it is not supported");;
                    }
                    break;
                case PacketType.VehiclePositionData:
                    {
                        logger.LogDebug("Received VehiclePositionData but it is not received");
                    }
                    break;
                case PacketType.PedPositionData:
                    {
                        var len = msg.ReadInt32();
                        var pedPosData = Util.DeserializeBinary<PedData>(msg.ReadBytes(len));
                        if (pedPosData != null)
                        {
                            var pedPluginResult = GameEvents.PedDataUpdate(client, pedPosData);
                            if (!pedPluginResult.ContinueServerProc) return;
                            pedPosData = pedPluginResult.Data;

                            pedPosData.Id = client.NetConnection.RemoteUniqueIdentifier;
                            pedPosData.Name = client.DisplayName;
                            pedPosData.Latency = client.Latency;

                            client.Health = pedPosData.PlayerHealth;
                            client.LastKnownPosition = pedPosData.Position;
                            client.IsInVehicle = false;

                            SendToAll(pedPosData, PacketType.PedPositionData, false, client);
                        }
                    }
                    break;
                case PacketType.ObjectSyncData:
                    {
                        var len = msg.ReadInt32();
                        var objSyncData = Util.DeserializeBinary<ObjectData>(msg.ReadBytes(len));
                        if (objSyncData != null) {
                            SendToAll(objSyncData, PacketType.ObjectSyncData, false, client);
                        }
                    }
                    break;
                case PacketType.NpcVehPositionData:
                    {
                        logger.LogDebug("Received NpcVehPositionData but it is not supported");
                    }
                    break;
                case PacketType.NpcPedPositionData:
                    {
                        logger.LogDebug("Received NpcPedPositionData but it is not supported");
                    }
                    break;
                case PacketType.WorldSharingStop:
                    {
                        GameEvents.WorldSharingStop(client);
                        var dcObj = new PlayerDisconnect()
                        {
                            Id = client.NetConnection.RemoteUniqueIdentifier
                        };
                        SendToAll(dcObj, PacketType.WorldSharingStop, true);
                    }
                    break;
                case PacketType.NativeResponse:
                    {
                        logger.LogDebug("Received NativeResponse packet but it is not supported");
                    }
                    break;
                case PacketType.PlayerSpawned:
                    {
                        GameEvents.PlayerSpawned(client);
                        logger.LogInformation("Player spawned: " + client.DisplayName);
                    }
                    break;
                // The following is normally only received on the client.
                case PacketType.PlayerDisconnect:
                    break;
                case PacketType.DiscoveryResponse:
                    break;
                case PacketType.ConnectionRequest:
                    break;
                case PacketType.NativeCall:
                    break;
                case PacketType.NativeTick:
                    break;
                case PacketType.NativeTickRecall:
                    break;
                case PacketType.NativeOnDisconnect:
                    break;
                case PacketType.NativeOnDisconnectRecall:
                    break;
                default:
                    // ReSharper disable once NotResolvedInText
                    // resharper wants to see a variable name in the below... w/e.
                    throw new ArgumentOutOfRangeException("Received unknown packet type. Server out of date or modded client?");
            }
        }

        public void SendToAll(object dataToSend, PacketType packetType, bool packetIsImportant)
        {
            var data = Util.SerializeBinary(dataToSend);
            var msg = Server.CreateMessage();
            msg.Write((int)packetType);
            msg.Write(data.Length);
            msg.Write(data);
            Server.SendToAll(msg, packetIsImportant ? NetDeliveryMethod.ReliableOrdered : NetDeliveryMethod.ReliableSequenced);
        }

        public void SendToAll(object dataToSend, PacketType packetType, bool packetIsImportant, Client clientToExclude)
        {
            var data = Util.SerializeBinary(dataToSend);
            var msg = Server.CreateMessage();
            msg.Write((int)packetType);
            msg.Write(data.Length);
            msg.Write(data);
            Server.SendToAll(msg, clientToExclude.NetConnection, packetIsImportant ? NetDeliveryMethod.ReliableOrdered : NetDeliveryMethod.ReliableSequenced, GetChannelForClient(clientToExclude));
        }

        public void DenyConnect(Client player, string reason, bool silent = true, NetIncomingMessage msg = null,
            int duraction = 60)
        {
            player.NetConnection.Deny(reason);
            logger.LogInformation($"Player rejected from server: {player.DisplayName} for {reason}");
            if (!silent)
            {
                SendNotificationToAll($"Player rejected by server: {player.DisplayName} - {reason}");
            }

            Clients.Remove(player);
            if (msg != null) Server.Recycle(msg);
        }

        public int GetChannelForClient(Client c)
        {
            lock (Clients) return (Clients.IndexOf(c) % 31) + 1;
        }

        // Stuff for scripting

        // Notification stuff
        public void SendNotificationToPlayer(Client player, string message, bool flashing = false)
        {
            throw new NotImplementedException();
        }

        public void SendNotificationToAll(string message, bool flashing = false)
        {
            throw new NotImplementedException();
        }


        public void KickPlayer(Client player, string reason = null, bool silent = false, Client sender = null)
        {
            player.Kicked = true;
            player.KickReason = reason?.ToString();
            player.Silent = silent;
            player.KickedBy = sender;
            player.NetConnection.Disconnect("Kicked: " + reason);
        }

        public void SetPlayerPosition(Client player, Vector3 newPosition)
        {
            throw new NotImplementedException();
        }

        public void GetPlayerPosition(Client player, Action<object> callback, string salt = "salt") {
            throw new NotImplementedException();
        }
    }
}

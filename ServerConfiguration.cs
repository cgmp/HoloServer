﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;

namespace HoloServer
{
    [XmlType(TypeName = "ServerVariable")]
    public struct ServerVariable
    {
        public string Key { get; set; }
        public string Value { get; set; }
    }

    public class ServerConfiguration
    {
        public int Port { get; set; } = 4499;
        public int MaxClients { get; set; } = 16;
        public string GamemodeName { get; set; } = "freeroam";
        public string ServerName { get; set; } = "HoloSync Server";
        public string Password { get; set; } = "";
        public bool AllowNicknames { get; set; } = true;
        public bool AllowOutdatedClients { get; set; } = false;
        public bool DebugMode { get; set; } = false;

        public List<string> ServerPlugins { get; set; } = new List<string>() {};

        public List<ServerVariable> ServerVariables = new List<ServerVariable>();
    }
}
